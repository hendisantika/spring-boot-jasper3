package com.hendisantika.springbootjasper3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJasper3Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJasper3Application.class, args);
	}
}
